/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package catalog;

/**
 *
 * @author Khazetdinov Artur
 */
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author archee
 */
public class Catalog {
    public static void main(String args[]){
        final JFrame frame = new JFrame();//создание фрейма
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//закрывание проги и выгрузка из оперативы нажатием на крестик
        JPanel panel=new JPanel();//создание панели
        frame.setSize(500,500);//размер окна
        frame.setTitle("Catalog");//имя окна
        frame.setResizable(false);//пользователь не имеет право изменять размер окна
        frame.setLocation(500,100);//расположение фрейма
        frame.add(panel);//добавление панели
        final JTextField name=new JTextField(35);//создание формы для имени
        String [] manufacters= new String [] {"Manufacter","Russia", "USA", "China", "Japan"};//объект с производителями
        final JComboBox manufacter = new JComboBox(manufacters);//создание формы для производителя
        final JTextField weight=new JTextField(35);//создание формы для веса
        final JTextField date=new JTextField(35);//создание формы для даты
        final JTextField category=new JTextField(35);//создание формы для категории
        Label lname =new Label("Name :");
        Label lmanufacter =new Label("Manufacter :");
        Label lweight =new Label("Weight(gm) :");
        Label ldate =new Label("Date(DD/MM/YYYY) :");
        Label lcategory =new Label("Category :");
        final JButton button = new JButton("Add");
        final JButton button1 = new JButton("Output");
        panel.setLayout(new GridLayout(0,1));
        panel.add(lname);
        panel.add(name);//добавление формы для имени
        panel.add(lmanufacter);
        panel.add(manufacter);//добавление формы для производителя
        panel.add(lweight);
        panel.add(weight);//добавление формы для веса
        panel.add(ldate);
        panel.add(date);//добавление формы для даты
        panel.add(lcategory);
        panel.add(category);//добавление формы для категории
        panel.add(button);//добавление кнопки отправки
        panel.add(button1);
        final ArrayList<String> catalog = new ArrayList<String>();
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name1=checkName(name.getText());
                String manufacter1=checkManufacter((String)manufacter.getSelectedItem());
                String weight1=checkWeight(weight.getText());
                String date1=checkDate(date.getText());
                String category1=checkCategory(category.getText());
                if(name1!="0"&& weight1!="0" && manufacter1!="0" && date1!="0" && category1!="0"){ 
                    catalog.add("Name: " +name1+";  Manufacter: " +manufacter1+";  Weight: "+weight1+"gm"+";  Date: "+date1+ "; Category: " +category1+";");
                    JOptionPane.showMessageDialog(null, "Данные сохранены!");
                }
                
            }
       });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(catalog.isEmpty()){
                     JOptionPane.showMessageDialog(null, "Данных нет!");
                }else{ 
                         JOptionPane.showMessageDialog(null, catalog);
                }
            }
          });    
       
        
        frame.setVisible(true);//видимый фрейм
        
    }
    public static String checkName(String name) {//проверка названия
        if(name !=""){
            return name; 
        }else{  
            JOptionPane.showMessageDialog(null, "Введите название!");
            return "0";
            
        }
    }
    
     public static String checkDate(String date){  //проверка даты
        Pattern p = Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)");  
        Matcher m = p.matcher(date); 
        if(m.matches() == true){
            return date;
        }else{
          JOptionPane.showMessageDialog(null, "Неверно введена дата изготовления!");
          return "0"; 
        }
    } 
     public static String checkWeight(String weight) {//проверка веса
        Pattern p = Pattern.compile("^\\d+$");  
        Matcher m = p.matcher(weight); 
        if(m.matches() == true){
            return weight;
        }else{
        JOptionPane.showMessageDialog(null, "Неверно введен вес!");
        return "0"; 
        }
    }
 public static String checkManufacter(String manufacter) {//проверка производителя
        if(manufacter == "Manufacter"){
            JOptionPane.showMessageDialog(null, "Выберите производителя!");
            return "0";
            
        }else{ 
            
            return manufacter; 
        }
    }
 public static String checkCategory(String category) {//проверка категории
        if(category !=""){
            return category; 
        }else{  
             JOptionPane.showMessageDialog(null, "Введите категорию!");
            return "0";
        }
    }  
}
