/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientservergame;

/**
 *
 * @author Khazetdinov Artur
 */
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;

public class Server implements Runnable {

    static private ServerSocket server;
    static private Socket connection;
    static private ObjectOutputStream output;
    static private ObjectInputStream input;

    @Override
    public void run() {
        try {
            server = new ServerSocket(8080, 10);
            int number = (int) (Math.random() * 10);
            while (true) {
                connection = server.accept();
                output = new ObjectOutputStream(connection.getOutputStream());
                input = new ObjectInputStream(connection.getInputStream());
                Object n = input.readObject();
                int number2 = Integer.parseInt(n.toString());
                if (number == number2) {
                    output.writeObject("Вы угадали число: " + number2 + "  Мы загадали новое число");
                    number = (int) (Math.random() * 10);
                } else if (number > number2) {
                    output.writeObject("Больше ");

                } else if (number < number2) {
                    output.writeObject("Меньше");
                }

            }
        } catch (UnknownHostException e) {
        } catch (IOException e) {
        } catch (HeadlessException e) {
        } catch (ClassNotFoundException e) {
        }

    }

}


