/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notebook.utils;

/**
 *
 * @author Khazetdinov Artur
 */
public class UserInteractorException extends Exception {

  public UserInteractorException() {}

  public UserInteractorException(String msg) {
    super(msg);
  }
}
