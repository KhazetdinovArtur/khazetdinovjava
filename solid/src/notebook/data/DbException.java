/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notebook.data;

/**
 *
 * @author Khazetdinov Artur
 */
public class DbException extends Exception {

  public DbException() {}

  public DbException(String msg) {
    super(msg);
  }
}
