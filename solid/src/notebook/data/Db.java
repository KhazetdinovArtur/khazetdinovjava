/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notebook.data;

/**
 *
 * @author Khazetdinov Artur
 */
public interface Db {
  public void save(Object obj) throws DbException;
  public Object[] findAll() throws DbException;
}
