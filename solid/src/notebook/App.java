/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notebook;

/**
 *
 * @author Khazetdinov Artur
 */
import notebook.data.Db;
import notebook.data.DbException;
import notebook.data.FileDb;
import notebook.utils.UserInteractor;
import notebook.utils.PunchedCardUserInteractor;
import notebook.utils.UserInteractorException;
import notebook.utils.UserInteractorReadException;
import notebook.utils.UserInteractorWriteException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;
import notebook.utils.ConsoleUserInteractor;


public class App extends Application{
  
  private static final String DB = "D:\\db.txt";
  //private static final String PUNCH_CARD = "D:\\card.txt";
  private static String text="";
  
  protected UserInteractor terminal;
  protected Db db;

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    App app = new App(args);
    text=sc.next();
  }

  public App(String[] args) {
    super(args);
    
  }

  @Override
  public void init() {
    try {
      
       this.terminal= new ConsoleUserInteractor(Paths.get(App.text));
      //this.terminal = new PunchedCardUserInteractor(Paths.get(App.PUNCH_CARD));
      this.db = new FileDb(App.DB);
    } catch (UserInteractorException ex) {
      System.out.println("Couldn't start application due error:");
      System.err.println(ex.getMessage());
      System.exit(1);
    }
  }

  @Override
  public void start() {
    try {
      String command;
      while((command = this.terminal.readCommand()) != null){
        switch(command){
          case "readAll":
            this.terminal.print(Arrays.toString(this.db.findAll()));
            break;
          case "save":
            if((command = this.terminal.readCommand()) != null){
              this.db.save(command);
            }
            break;
          default:
            this.terminal.print("Unkown command");
        }
//        this.terminal.print(command);
      }
    } catch (UserInteractorReadException ex) {
      System.out.println("Can't read user input due error:");
      System.err.println(ex.getMessage());
      System.exit(1);
    } catch (UserInteractorWriteException ex) {
      System.out.println("Can't print data to user due error:");
      System.err.println(ex.getMessage());
      System.exit(1);
    } catch (DbException ex) {
      System.err.println(ex.getMessage());
      System.exit(1);
    }
  }
  
}
