/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weatherwidget;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author archee
 */
/**
 *
 * @author archee
 */
public class WeatherKazan extends javax.swing.JFrame {

    public WeatherKazan() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {
        GetWeather gw = null;
        try {
            gw = new GetWeather();
        } catch (IOException ex) {
            Logger.getLogger(WeatherKazan.class.getName()).log(Level.SEVERE, null, ex);
        }

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(191, 239, 255));

        jLabel1.setFont(new java.awt.Font("URW Palladio L", 2, 48)); // NOI18N
        jLabel1.setText("Казань");

        jLabel2.setText("Сейчас:");

        jLabel3.setText(gw.time1);

        jLabel4.setFont(new java.awt.Font("URW Palladio L", 0, 48)); // NOI18N
        jLabel4.setText(gw.temp1_now);

        jLabel5.setText("Вечером:");

        jLabel6.setText(gw.temp1_evening);

        jLabel7.setText("Ночью:");

        jLabel8.setText(gw.temp1_night);

        jLabel9.setText(gw.type1);

        jLabel14.setText("Ветер:");

        jLabel15.setText(gw.wind1);

        jLabel16.setText("Влажность:");

        jLabel17.setText(gw.humidity1);

        jLabel18.setText("Давление:");

        jLabel19.setText(gw.pressure1);

        jButton1.setText("Обновить");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                final String time1;//время
                final String type1;//тип погоды
                final String temp1_now;//температура сейчас
                final String temp1_evening;//температура вечером
                final String temp1_night;//температура ночью
//    final String sunrise1;//восход
//    final String setting1;//заход
                final String wind1;//ветер
                final String humidity1;//влажность
                final String pressure1;//давление

                Document doc = null;
                Document doc1 = null;
                try {
                    doc1 = Jsoup.connect("http://116.ru/weather/Kazan/").get();
                } catch (IOException ex) {
                    Logger.getLogger(WeatherKazan.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    doc = Jsoup.connect("https://pogoda.yandex.ru/kazan").get();
                } catch (IOException ex) {
                    Logger.getLogger(WeatherKazan.class.getName()).log(Level.SEVERE, null, ex);
                }
                //Время
                Elements time = doc.getElementsByClass("current-weather__local-time");
                System.out.println("Время: " + (String) time.text());

                //тип погоды
                Elements type = doc.getElementsByClass("current-weather__comment");
                System.out.println("Тип погоды: " + type.text());

                //температура сейчас
                Elements temp_now = doc.getElementsByClass("current-weather__thermometer_type_now");
                System.out.println("Температура сейчас: " + temp_now.text());

                //температура вечером
                Elements temp_evening = doc.getElementsByClass("current-weather__thermometer_type_after");
                String b = temp_evening.text();
                String b1 = "";
                b1 = b1 + b.charAt(0) + b.charAt(1);

                System.out.println("Температура вечером: " + b1);

                //температура ночью
                Elements temp_night = doc.getElementsByClass("current-weather__thermometer_type_after");
                String c = temp_evening.text();
                String c1 = "";
                c1 = c1 + c.charAt(3) + c.charAt(4);

                System.out.println("Температура сейчас: " + c1);

//        //восход
//        Elements sunrise = doc.getElementsByClass("current-weather__info-label");
//        System.out.println("Восход: " + sunrise.text());
//        
//        //Заход
//        Elements setting = doc.getElementsByClass("current-weather__thermometer_type_now");
//        System.out.println("Заход: " + setting.text());
                //ветер
                Elements wind = doc.getElementsByClass("wind-speed");
                String a = wind.text();
                String a1 = "";

                for (int i = 0; i < a.length(); i++) {
                    if (a.charAt(i) != 'с') {
                        a1 = a1 + a.charAt(i);
                    } else {
                        break;
                    }
                }

                //влажность
                Elements humidity = doc1.getElementsByTag("span");
                String e = humidity.text();
                String e1 = "";
                for (int i = 130; i < 135; i++) {
                    e1 = e1 + e.charAt(i);
                }
                System.out.println("Влажность: " + e1);

//        //Давление
                Elements pressure = doc1.getElementsByTag("span");
                String d = pressure.text();
                String d1 = "";
                for (int i = 116; i < 129; i++) {
                    d1 = d1 + d.charAt(i);
                }
                System.out.println("Давление: " + d1);
                time1 = time.text();
                type1 = type.text();
                temp1_now = temp_now.text();
                temp1_evening = b1 + "°C";
                temp1_night = c1 + "°C";
//        sunrise1=sunrise.text();
//        setting1=setting.text();
                wind1 = a1 + "с";
                humidity1 = e1;
                pressure1 = d1;
                jLabel3.setText(time1);
                jLabel4.setText(temp1_now);
                jLabel6.setText(temp1_evening);
                jLabel8.setText(temp1_night);
                jLabel9.setText(type1);
                jLabel15.setText(wind1);
                jLabel17.setText(humidity1);
                jLabel19.setText(pressure1);

            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(jLabel2)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(jLabel3)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(jLabel14)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jLabel15))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(jLabel16)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jLabel17))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(jLabel18)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(jLabel19)))
                                        .addGap(92, 92, 92))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                                        .addComponent(jLabel5)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jLabel6))
                                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                                        .addComponent(jLabel7)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jLabel8)))
                                                        .addGap(83, 83, 83)
                                                        .addComponent(jButton1))
                                                .addComponent(jLabel9))
                                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel2)
                                                .addComponent(jLabel3)))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel14)
                                                .addComponent(jLabel15))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel16)
                                                .addComponent(jLabel17))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel18)
                                                .addComponent(jLabel19))))
                        .addGap(11, 11, 11)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel5)
                                                .addComponent(jLabel6))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel7)
                                                .addComponent(jLabel8))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                                        .addComponent(jButton1)
                                        .addGap(29, 29, 29))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                        

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WeatherKazan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WeatherKazan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WeatherKazan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WeatherKazan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new WeatherKazan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration                   
}
