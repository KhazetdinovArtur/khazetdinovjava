/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paintswing;

/**
 *
 * @author Khazetdinov Artur
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import static java.lang.Math.abs;
import javax.swing.JComponent;

public class PaintSwing extends JComponent {

    private Image image;
    private Graphics2D g2;
    private int currentX, currentY, oldX, oldY, oldX1, oldY1;

    public PaintSwing() {
        setDoubleBuffered(true);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                oldX = e.getX();
                oldY = e.getY();
                oldX1 = e.getX();
                oldY1 = e.getY();

            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                currentX = e.getX();
                currentY = e.getY();
                if (g2 != null) {
                    g2.drawLine(oldX, oldY, currentX, currentY);
                    repaint();
                    oldX = currentX;
                    oldY = currentY;
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (image == null) {
            image = createImage(getSize().width, getSize().height);
            g2 = (Graphics2D) image.getGraphics();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            clear();
        }
        g.drawImage(image, 0, 0, null);
    }

    public void figure() {
        g2.setColor(Color.red);
        if (oldX1 <= oldX && oldY1 <= oldY) {
            g2.fillRect(oldX1, oldY1, abs(oldX1 - oldX), abs(oldY1 - oldY));
        }
        if (oldX1 > oldX && oldY1 <= oldY) {
            g2.fillRect(oldX1 - abs(oldX1 - oldX), oldY1, abs(oldX1 - oldX), abs(oldY1 - oldY));
        }
        if (oldX1 <= oldX && oldY1 > oldY) {
            g2.fillRect(oldX1, oldY1 - abs(oldY1 - oldY), abs(oldX1 - oldX), abs(oldY1 - oldY));
        }
        if (oldX1 > oldX && oldY1 > oldY) {
            g2.fillRect(oldX1 - abs(oldX1 - oldX), oldY1 - abs(oldY1 - oldY), abs(oldX1 - oldX), abs(oldY1 - oldY));
        }
           
        repaint();
    }

    public void clear() {
        g2.setPaint(Color.white);
        g2.fillRect(0, 0, getSize().width, getSize().height);
        g2.setPaint(Color.black);
        repaint();
    }

    public void black() {
        g2.setPaint(Color.black);
    }

    public void red() {
        g2.setPaint(Color.red);
    }

    public void green() {
        g2.setPaint(Color.green);
    }

    public void blue() {
        g2.setPaint(Color.blue);
    }

}

