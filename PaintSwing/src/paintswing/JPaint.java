/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paintswing;

/**
 *
 * @author Artur Khazetdinov
 */
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class JPaint {

    JButton clearBtn, blackBtn, blueBtn, greenBtn, redBtn, ovalBtn;
    PaintSwing paint;
    ActionListener actionListener = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == ovalBtn) {
                paint.figure();
            }
            if (e.getSource() == clearBtn) {
                paint.clear();
            } else if (e.getSource() == blueBtn) {
                paint.blue();
            } else if (e.getSource() == blackBtn) {
                paint.black();
            } else if (e.getSource() == greenBtn) {
                paint.green();
            } else if (e.getSource() == redBtn) {
                paint.red();
            }
        }
    };

    public static void main(String[] args) {
        new JPaint().show();
    }

    public void show() {
        JFrame frame = new JFrame("JPaint");
        Container content = frame.getContentPane();
        content.setLayout(new BorderLayout());
        paint = new PaintSwing();
        content.add(paint, BorderLayout.CENTER);
        JPanel controls = new JPanel();
        ovalBtn = new JButton("Квадрат");
        ovalBtn.addActionListener(actionListener);
        clearBtn = new JButton("Clear");
        clearBtn.addActionListener(actionListener);
        blackBtn = new JButton(new ImageIcon("black.png"));
        blackBtn.addActionListener(actionListener);
        blueBtn = new JButton(new ImageIcon("blue.png"));
        blueBtn.addActionListener(actionListener);
        greenBtn = new JButton(new ImageIcon("green.png"));
        greenBtn.addActionListener(actionListener);
        redBtn = new JButton(new ImageIcon("red.png"));
        redBtn.addActionListener(actionListener);
        controls.add(blackBtn);
        controls.add(greenBtn);
        controls.add(blueBtn);
        controls.add(redBtn);
        controls.add(clearBtn);
        controls.add(ovalBtn);
        content.add(controls, BorderLayout.NORTH);
        frame.setSize(600, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}

