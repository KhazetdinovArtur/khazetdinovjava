/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weatherwidget;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author archee
 */
class GetWeather {

    final String time1;//время
    final String type1;//тип погоды
    final String temp1_now;//температура сейчас
    final String temp1_evening;//температура вечером
    final String temp1_night;//температура ночью
//    final String sunrise1;//восход
//    final String setting1;//заход
    final String wind1;//ветер
    final String humidity1;//влажность
    final String pressure1;//давление

    public GetWeather() throws IOException {

        Document doc = null;
        Document doc1=null;
        doc1=Jsoup.connect("http://116.ru/weather/Kazan/").get();
        doc = Jsoup.connect("https://pogoda.yandex.ru/kazan").get();
        //Время
        Elements time = doc.getElementsByClass("current-weather__local-time");
        System.out.println("Время: " + (String) time.text());

        //тип погоды
        Elements type = doc.getElementsByClass("current-weather__comment");
        System.out.println("Тип погоды: " + type.text());

        //температура сейчас
        Elements temp_now = doc.getElementsByClass("current-weather__thermometer_type_now");
        System.out.println("Температура сейчас: " + temp_now.text());

        //температура вечером
        Elements temp_evening = doc.getElementsByClass("current-weather__thermometer_type_after");
        String b = temp_evening.text();
        String b1 = "";
        b1=b1+b.charAt(0)+b.charAt(1);

                System.out.println("Температура вечером: " + b1);

        
        //температура ночью
        Elements temp_night = doc.getElementsByClass("current-weather__thermometer_type_after");
        String c = temp_evening.text();
        String c1="";
        c1 = c1+c.charAt(3)+c.charAt(4);

        
        System.out.println("Температура сейчас: " + c1);
        
//        //восход
//        Elements sunrise = doc.getElementsByClass("current-weather__info-label");
//        System.out.println("Восход: " + sunrise.text());
//        
//        //Заход
//        Elements setting = doc.getElementsByClass("current-weather__thermometer_type_now");
//        System.out.println("Заход: " + setting.text());
        //ветер
        Elements wind = doc.getElementsByClass("wind-speed");
        String a = wind.text();
        String a1 = "";

        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) != 'с') {
                a1 = a1 + a.charAt(i);
            } else {
                break;
            }
        }

        //влажность
         
        
        Elements humidity = doc1.getElementsByTag("span");
        String e=humidity.text();
        String e1="";
        for(int i=130;i<135;i++){
            e1=e1+e.charAt(i);
        }
        System.out.println("Влажность: " + e1);
        
//        //Давление
        Elements pressure = doc1.getElementsByTag("span");
        String d=pressure.text();
        String d1="";
        for(int i=116;i<129;i++){
            d1=d1+d.charAt(i);
        }
          System.out.println("Давление: " + d1);
        time1 = time.text();
        type1 = type.text();
        temp1_now = temp_now.text();
        temp1_evening=b1+"°C";
        temp1_night=c1+"°C";
//        sunrise1=sunrise.text();
//        setting1=setting.text();
        wind1 =  a1 + "с";
        humidity1=e1;
        pressure1=d1;

    }
}
