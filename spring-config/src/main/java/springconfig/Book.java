/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package springconfig;
import org.springframework.stereotype.Component;
/**
 *
 * @author archee
 */
@Component
public class Book {
  private String name = "The Book";
  private String isbn = "0000000000000";

  public void setName(String name) {
    this.name = name;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }
    
  public String getName(){
    return name;
  }

  public String getIsbn() {
    return isbn;
  }
  
  public String toString(){
    return getName()+" "+getIsbn();
  }
  
}
