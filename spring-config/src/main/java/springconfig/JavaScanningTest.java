/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package springconfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author archee
 */
public class JavaScanningTest {

    public static void main(String[] args) {
        System.out.println("Java config context configuration test");

        ApplicationContext context
                = new AnnotationConfigApplicationContext(JavaScannigConfigTest.class);

        Book obj = (Book) context.getBean("book1");
        System.out.println(obj);

        obj.setName("Spring in Action");
        obj.setIsbn("9781935182351");
        System.out.println(obj);
    }
}
