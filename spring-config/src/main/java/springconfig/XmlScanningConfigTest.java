/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package springconfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
/**
 *
 * @author archee
 */
public class XmlScanningConfigTest {

  public static void main(String[] args) {
    new XmlScanningConfigTest();
  }
  
  public XmlScanningConfigTest(){
    System.out.println("Scanning context configuration test");
    
    ApplicationContext context
      = new FileSystemXmlApplicationContext(getClass().getResource("/itisScanningConfig.xml").toString());
    
    Book obj = (Book) context.getBean("book");
    System.out.println( obj );
//     Book obj1 = (Book) context.getBean("book1");
//    System.out.println( obj );
//     Book obj2 = (Book) context.getBean("book2");
//    System.out.println( obj );
    
    obj.setName("Spring in Action");
    obj.setIsbn("9781935182351");
    System.out.println( obj );
  }

}
