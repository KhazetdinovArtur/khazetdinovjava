/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AD;

/**
 *
 * @author archee
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import records.Data;

/**
 * @author archee
 *
 */
public class ADReadDB {

    public static String[][] data = new String[100][3];
    private Connection connection;
    private ResultSet results;

    public ADReadDB(String dbName, String uname, String pwd) {
        String url = "jdbc:mysql://localhost:3306/" + dbName;

        // set up the driver
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            this.connection = DriverManager.getConnection(url, uname, pwd);
        } catch (InstantiationException e) {

            e.printStackTrace();
        } catch (IllegalAccessException e) {

            e.printStackTrace();
        } catch (ClassNotFoundException e) {

            e.printStackTrace();
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    public void doRead() {
       
        String query = "select * from SberbankD "+"select * from MasterD "+"select * from YandexD";

        try {
            PreparedStatement ps = this.connection.prepareStatement(query);

            this.results = ps.executeQuery();

        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    
    public Data getData() {

        data = new String[100][3];
        Data data1 = new Data();
        int i = 0;
        int j = 0;
        try {

            while (this.results.next()) {
                data1.setId(this.results.getInt("ID"));
                data1.setPrice(this.results.getInt("price"));
                data1.setAbout(this.results.getString("about"));

                data[i][j] = "" + data1.getId() + "";
                j++;
                data[i][j] = "" + data1.getPrice() + "";
                j++;
                data[i][j] = data1.getAbout();

                i++;
                j = 0;

            }
        } catch (SQLException e) {

            e.printStackTrace();
        }

        return data1;
    }

}

