/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SD;

/**
 *
 * @author archee
 */
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import records.Data;

public class SDDataBase {

    static JFrame frame = new JFrame("Сбербанк Доходы");
    public static void createGUI() {
        frame.setLayout(null);
        String[] columnNames = {
            "Id",
            "Price",
            "About"
        };

        SDReadDB rdb = new SDReadDB("Records", "root", "fhneh112233");
        rdb.doRead();
        rdb.getData();
        Data data1 = new Data();

        String[][] data = SDReadDB.data;

        JTable table = new JTable(data, columnNames);

        JScrollPane scrollPane = new JScrollPane(table);
        
      
        scrollPane.setBounds(1, 1, 750, 200);
        
        frame.getContentPane().add(scrollPane);
        frame.setPreferredSize(new Dimension(760, 260));
        frame.pack();
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

 

   
}

