/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package records;

import AR.ARDataBase;
import AD.ADDataBase;
import YR.YRDataBase;
import YD.YDDataBase;
import MR.MRDataBase;
import SR.SRDataBase;
import MD.MDDataBase;
import SD.SDDataBase;
import java.util.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 *
 * @author archee
 */
public class Records {

    public static void main(String[] args) {
        RecordsFrame frame = new RecordsFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

class RecordsFrame extends JFrame {

    public RecordsFrame() {
        setSize(300, 600);
        setTitle("Records");
        RecordsPanel panel = new RecordsPanel();
        add(panel);
    }
}

class RecordsPanel extends JPanel {

    public RecordsPanel() {
        JButton sber = new JButton(new ImageIcon("sberbank.jpg"));
        JButton master = new JButton(new ImageIcon("03-mastercard-w300.png"));
        JButton yandex = new JButton(new ImageIcon("images.jpeg"));
        JButton all = new JButton(new ImageIcon("j1.jpg"));
        setLayout(new GridLayout(0, 1));
        add(sber);
        add(master);
        add(yandex);
        add(all);
        sber.addActionListener(new ActionListener() {/*сбербанк*/
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame sframe = new JFrame();
                sframe.setSize(300, 300);
                sframe.setTitle("Сбербанк");
                JPanel panel1 = new JPanel();
                sframe.add(panel1);
                JButton dohod = new JButton("Доходы");
                JButton rashod = new JButton("Расходы");
                panel1.setLayout(new GridLayout(0, 1));
                panel1.add(dohod);
                panel1.add(rashod);
                sframe.setVisible(true);
                dohod.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                            SDDataBase sd = new SDDataBase();
                            sd.createGUI();
                    }
                });
                rashod.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                            SRDataBase sr = new SRDataBase();
                            sr.createGUI();
                    }
                });

            }
        });
        master.addActionListener(new ActionListener() {/*Мастеркард*/
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame sframe = new JFrame();
                sframe.setSize(300, 300);
                sframe.setTitle("Мастер кард");
                JPanel panel1 = new JPanel();
                sframe.add(panel1);
                JButton dohod = new JButton("Доходы");
                JButton rashod = new JButton("Расходы");
                panel1.setLayout(new GridLayout(0, 1));
                panel1.add(dohod);
                panel1.add(rashod);
                sframe.setVisible(true);
                dohod.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                            MDDataBase md = new MDDataBase();
                            md.createGUI();
                    }
                });
                rashod.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                            MRDataBase mr = new MRDataBase();
                            mr.createGUI();
                    }
                });
                

            }
        });
        yandex.addActionListener(new ActionListener() {/*Яндекс*/
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame sframe = new JFrame();
                sframe.setSize(300, 300);
                sframe.setTitle("Яндекс.Деньги");
                JPanel panel1 = new JPanel();
                sframe.add(panel1);
                JButton dohod = new JButton("Доходы");
                JButton rashod = new JButton("Расходы");
                panel1.setLayout(new GridLayout(0, 1));
                panel1.add(dohod);
                panel1.add(rashod);
                sframe.setVisible(true);
                dohod.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                            YDDataBase yd = new YDDataBase();
                            yd.createGUI();
                    }
                });
                rashod.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                           YRDataBase yr = new YRDataBase();
                           yr.createGUI();
                    }
                });

            }
        });
        all.addActionListener(new ActionListener() {/*общаг*/
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame sframe = new JFrame();
                sframe.setSize(300, 300);
                sframe.setTitle("Общаяя таблица");
                JPanel panel1 = new JPanel();
                sframe.add(panel1);
                JButton dohod = new JButton("Доходы");
                JButton rashod = new JButton("Расходы");
                panel1.setLayout(new GridLayout(0, 1));
                panel1.add(dohod);
                panel1.add(rashod);
                sframe.setVisible(true);
                dohod.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                            ADDataBase ad = new ADDataBase();
                            ad.createGUI();
                    }
                });
                rashod.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                            ARDataBase ar = new ARDataBase();
                            ar.createGUI();
                    }
                });

            }
        });
    }
}
