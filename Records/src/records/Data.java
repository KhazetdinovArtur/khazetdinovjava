/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package records;

/**
 *
 * @author archee
 */
/**
 *
 * @author archee
 */
public class Data {

    private int ID;
    private int price;
    private String about;

    public Data() {
        this.ID = 1;
        this.price = 0;
        this.about = "no about";
    }

    /**
     * @return the id
     */
    public int getId() {
        return ID;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.ID = ID;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the about
     */
    public String getAbout() {
        return about;
    }

    /**
     * @param about the about to set
     */
    public void setAbout(String about) {
        this.about = about;
    }

    public Data(int ID, int price, String about) {
        this.ID = ID;
        this.price = price;
        this.about = about;

    }

    @Override
    public String toString() {
        return "\"" + ID + "\"" + "," + "\"" + price + "\"" + "," + "\"" + about + "\"";
    }

}

