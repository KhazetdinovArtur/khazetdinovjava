package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import model.Users;

public final class addForm_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    Users users = (Users) request.getAttribute("user");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!--\n");
      out.write("To change this license header, choose License Headers in Project Properties.\n");
      out.write("To change this template file, choose Tools | Templates\n");
      out.write("and open the template in the editor.\n");
      out.write("-->\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>Registration</title>\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-2.1.4.js\"></script>\n");
      out.write("        <script type=\"text/javascript\"\n");
      out.write("        src=\"https://gist.githubusercontent.com/lucasmezencio/4145835/raw/c8729a32eee313fa16c4ed6ab2ce44bcdf6ff56d/jQuery.limit.js\"></script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <form name=addForm method=\"post\" action=addUser>\n");
      out.write("            <p><b>Login:</b><br>\n");
      out.write("                <input type=\"text\" size=\"40\" value=\"\"></p>\n");
      out.write("             <p><b>Password:(min 8)</b><br>\n");
      out.write("                 <input type=\"password\" size=\"40\"></p>\n");
      out.write("            <p><b>Sex:</b><Br>\n");
      out.write("                <input type=\"radio\" name=\"sex\" value=\"male\"> Male<Br>\n");
      out.write("                <input type=\"radio\" name=\"sex\" value=\"female\"> Female<Br>\n");
      out.write("            </p>\n");
      out.write("            <p><b>Subscription</b>\n");
      out.write("                <input type=\"checkbox\" name=subscription value=1>\n");
      out.write("            <p><b>About:</b><Br><br />Chars left: <b id=\"st\">300</b><br />\n");
      out.write("                <textarea name=\"comments\" cols=\"40\" rows=\"3\" maxlength=\"300\" spellcheck=\"false\"  onKeyUp=\"stat(this)\"></textarea></p>\n");
      out.write("            <p><input type=\"submit\" value=\"Enter\">\n");
      out.write("                <input type=\"reset\" value=\"Clear\"></p>\n");
      out.write("        </form>\n");
      out.write("        <script language=\"javascript\">\n");
      out.write("            function stat(stat) {\n");
      out.write("                st = document.getElementById(\"st\");\n");
      out.write("                st.textContent = 300 - stat.value.length;\n");
      out.write("            }\n");
      out.write("            ;\n");
      out.write("        </script> \n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
