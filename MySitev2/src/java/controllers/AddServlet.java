/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DB.AddQuery;
import DB.CheckClient;
import DB.CheckEmail;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Users;

/**
 *
 * @author Artur Khazetdinov
 */
@WebServlet(description = "Controller for adding new user for a database", urlPatterns = { "/addUser" })
public class AddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public AddServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get the data
		
		String url = "";
				String login = request.getParameter("login");				
				String password = request.getParameter("password");
				String sex= request.getParameter("sex");
				int subscription=0;
				if(request.getParameter("subscription") != null){
				 subscription = Integer.parseInt(request.getParameter("subscription"));
				}
				String comments = request.getParameter("comments");
				// set up a book object
				
				Users user = new Users();
				
				user.setLogin(login);
				user.setPassword(password);
				user.setSex(sex);
				user.setSubscription(subscription);
				user.setComments(comments);
				 if (CheckEmail.check(login)&&password.length()<10){
						url = "/logInForm.jsp";
						// set up an AddQuery object
						AddQuery aq = new AddQuery("servlet", "root", "");
						CheckClient csq = new CheckClient("servlet", "root", "",login);
						// pass the book to AddQuery to add to the database
						if (csq.checkDB(login)){
						aq.doAdd(user);
						}else{
							user.setLogin(login);
//							url = "/add"; user.setMessage("User is not in the base");
						}
					}else{
//						url = "/add"; user.setMessage("Write login and password correctly");
					}
				
				// pass execution control to the ReadServlet
			
				RequestDispatcher dispatcher = request.getRequestDispatcher(url);
				dispatcher.forward(request, response);
	}

}
