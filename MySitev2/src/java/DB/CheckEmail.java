/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Artur Khazetdinov
 */
public class CheckEmail {
    public static final Pattern VALID_EMAIL = Pattern.compile(
			"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	public static boolean check(String emailStr) {
		Matcher matcher = VALID_EMAIL.matcher(emailStr);
		return matcher.find();

	}
}
