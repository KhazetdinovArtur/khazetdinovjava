/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Users;

/**
 *
 * @author Artur Khazetdinov
 */
public class CheckClient {
	private Connection connection;
	private ResultSet results;

	private Users user= new Users();
	private String login;

	public  CheckClient(String dbName, String uname, String pwd, String login) {

		String url = "jdbc:mysql://localhost:3306/" + dbName;
		this.login = login;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			this.connection = DriverManager.getConnection(url, uname, pwd);
		} 
		
                catch(InstantiationException e){
                    e.printStackTrace();
                }
                catch(IllegalAccessException e){
                    e.printStackTrace();
                }
                catch(ClassNotFoundException e){
                    e.printStackTrace();
                }
                catch(SQLException e){
                    e.printStackTrace();
                }
                
	}

	public boolean checkDB(String login) {
		String query = "select * from users where login =?";

		try {
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setString(1, this.login);
			this.results = ps.executeQuery();

			while (results.next()) {
				
				user.setLogin(this.results.getString("login"));
				return false;
			}

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			if (results != null) {
				try {
					results.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
				results = null;
			}
			
		}
		return true;
	}

	public Users getUser() {
		return this.user;
	}
}

