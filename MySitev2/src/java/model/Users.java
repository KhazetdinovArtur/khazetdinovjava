/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Artur Khazetdinov
 */
public class Users {
    private int userID;
	private String login;
	private String password;
	private String sex;
	private int subscription;
	private String comments;

	/**
	 * @default constructor
	 */
	public Users() {
		this.userID = 1;
		this.login = "No email";
		this.password = "No password";
		this.sex = "No sex";
		this.subscription = 0;
		this.comments = "No comments";
	}

	/**
	 * @return the userID
	 */
	public int getUserID() {
		return userID;
	}

	/**
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login
	 *            the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @param sex
	 *            the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * @return the subscription
	 */
	public int getSubscription() {
		return subscription;
	}

	/**
	 * @param subscription
	 *            the subscription to set
	 */
	public void setSubscription(int subscription) {
		this.subscription = subscription;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Users [userID=" + userID + ", login=" + login + ", password=" + password + ", sex=" + sex
				+ ", subscription=" + subscription + ", comments=" + comments + "]";
	}

	/**
	 * @param userID
	 * @param login
	 * @param sex
	 * @param subscription
	 */
	public Users(int userID, String login, String password, String sex, int subscription, String comments, String posts, String messages) {
		this.userID = userID;
		this.login = login;
		this.password = password;
		this.sex = sex;
		this.subscription = subscription;
		this.comments = comments;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the posts
	 */
	
}
