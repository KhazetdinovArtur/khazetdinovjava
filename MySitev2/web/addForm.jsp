<%-- 
    Document   : addForm
    Created on : 16.12.2015, 16:45:22
    Author     : Artur Khazetdinov
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ page import="model.Users"%>
<%
    Users users = (Users) request.getAttribute("user");
%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Registration</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.js"></script>
        <script type="text/javascript"
        src="https://gist.githubusercontent.com/lucasmezencio/4145835/raw/c8729a32eee313fa16c4ed6ab2ce44bcdf6ff56d/jQuery.limit.js"></script>
    </head>
    <body>
        <form name=addForm method="post" action=addUser>
            <p><b>Login:</b><br>
                <input type="text" size="40" value=""></p>
             <p><b>Password:</b><br>
                 <input type="password" name="password" value="" size="40"></p>
            <p><b>Sex:</b><Br>
                <input type="radio" name="sex" value="male"> Male<Br>
                <input type="radio" name="sex" value="female"> Female<Br>
            </p>
            <p><b>Subscription</b>
                <input type="checkbox" name=subscription value=1>
            <p><b>About:</b><Br><br />Chars left: <b id="st">300</b><br />
                <textarea name="comments" cols="40" rows="3" maxlength="300" spellcheck="false"  onKeyUp="stat(this)"></textarea></p>
            <p><input type="submit" value="Enter">
                <input type="reset" value="Clear"></p>
        </form>
        <script language="javascript">
            function stat(stat) {
                st = document.getElementById("st");
                st.textContent = 300 - stat.value.length;
            }
            ;
        </script> 
    </body>
</html>